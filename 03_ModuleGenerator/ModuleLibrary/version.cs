/******************************************************************************
 * @file      version.cs
 * @author    OSAR S.Reinemuth
 * @proj      ModuleLibrary
 * @date      Friday, January 1, 2021
 * @version   Application v. 1.3.2.7
 * @version   Generator   v. 1.2.5.1
 * @brief     Controls the assembly Version
 *****************************************************************************/
using System;
using System.Reflection;

[assembly: AssemblyVersion("1.3.2.7")]

namespace ModuleLibrary
{
  static public class ModuleLibraryVersionClass
  {
    public static int major { get; set; }  //Version of the program
    public static int minor { get; set; }  //Sub version of the program
    public static int patch { get; set; }  //Debug patch of the program
    public static int build { get; set; }  //Count program builds

    static ModuleLibraryVersionClass()
    {
			major = 1;
			minor = 3;
			patch = 2;
			build = 7;
    }

    public static string getCurrentVersion()
    {
      return major.ToString() + '.' + minor.ToString() + '.' + patch.ToString() + '.' + build.ToString();
    }
  }
}
//!< @version 0.0.1	->	Initial Project Setup
//!< @version 1.3.0	->	Initial module generator DLL. Replace the previous module generator executable.
//!< @version 1.3.1	->	Adding bugfix in module internal behavior file generation.
//!< @version 1.3.2	->	Update dependency libraries
