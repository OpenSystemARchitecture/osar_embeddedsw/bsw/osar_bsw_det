﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuleLibrary;
using OsarResources.Generator;

namespace TestModuleLibrary
{
  class Program
  {
    static void Main(string[] args)
    {
      GenInfoType genInfo, allGenInfos = new GenInfoType();
      //Get Instance of module Library
      OsarModuleContextInterface moduleGen = ModuleContext.GetInstance();

      Console.WriteLine(">>>>>>>>>> Set Default Configuration <<<<<<<<<<");
      genInfo = moduleGen.GetViewModel(".\\..\\..\\Test\\DetModuleConfig.xml", "C:\\Temp\\TestModule").SetDefaultConfiguration();
      allGenInfos = OsarGenHelper.MergeGenInfoType(allGenInfos, genInfo);

      foreach(string str in genInfo.info)
      { Console.WriteLine(str); }

      foreach (string str in genInfo.log)
      { Console.WriteLine(str); }

      foreach (string str in genInfo.warning)
      { Console.WriteLine(str); }

      foreach (string str in genInfo.error)
      { Console.WriteLine(str); }


      Console.WriteLine(">>>>>>>>>> Validate Configuration <<<<<<<<<<");
      genInfo = moduleGen.GetViewModel(".\\..\\..\\Test\\DetModuleConfig.xml", "C:\\Temp\\TestModule").ValidateConfiguration();
      allGenInfos = OsarGenHelper.MergeGenInfoType(allGenInfos, genInfo);

      foreach (string str in genInfo.info)
      { Console.WriteLine(str); }

      foreach (string str in genInfo.log)
      { Console.WriteLine(str); }

      foreach (string str in genInfo.warning)
      { Console.WriteLine(str); }

      foreach (string str in genInfo.error)
      { Console.WriteLine(str); }


      Console.WriteLine(">>>>>>>>>> Generate Configuration <<<<<<<<<<");
      genInfo = moduleGen.GetViewModel(".\\..\\..\\Test\\DetModuleConfig.xml", "C:\\Temp\\TestModule").GenerateConfiguration();
      allGenInfos = OsarGenHelper.MergeGenInfoType(allGenInfos, genInfo);

      foreach (string str in genInfo.info)
      { Console.WriteLine(str); }

      foreach (string str in genInfo.log)
      { Console.WriteLine(str); }

      foreach (string str in genInfo.warning)
      { Console.WriteLine(str); }

      foreach (string str in genInfo.error)
      { Console.WriteLine(str); }



      Console.WriteLine(">>>>>>>>>> Summary <<<<<<<<<<");
      foreach (string str in allGenInfos.info)
      { Console.WriteLine(str); }

      foreach (string str in allGenInfos.log)
      { Console.WriteLine(str); }

      foreach (string str in allGenInfos.warning)
      { Console.WriteLine(str); }

      foreach (string str in allGenInfos.error)
      { Console.WriteLine(str); }

      Console.ReadKey();
    }
  }
}
