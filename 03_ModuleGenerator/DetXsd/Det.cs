﻿/*****************************************************************************************************************************
* @file        Det.cs
* @author      Reinemuth Sebastian
* @date        02-11-2017
* @brief       Det Module XSD Class to generate the DET XML File
* @version     1.0.0
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generic;

namespace DetXsd
{
  /**
    * @brief    Det module xsd class to generate an xml style sheet
    */
  public class Det
  {
    versionClass xmlFileVersion;
    UInt16 detModuleID;
    UInt16 detDeviationCount;
    SystemState detModuleUsage;
    SystemState detEnable;

    public versionClass XmlFileVersion
    {
      get
      {
        return xmlFileVersion;
      }

      set
      {
        xmlFileVersion = value;
      }
    }

    public ushort DetModuleID
    {
      get
      {
        return detModuleID;
      }

      set
      {
        detModuleID = value;
      }
    }

    public ushort DetDeviationCount
    {
      get
      {
        return detDeviationCount;
      }

      set
      {
        detDeviationCount = value;
      }
    }

    public SystemState DetModuleUsage
    {
      get
      {
        return detModuleUsage;
      }

      set
      {
        detModuleUsage = value;
      }
    }

    public SystemState DetEnable
    {
      get
      {
        return detEnable;
      }

      set
      {
        detEnable = value;
      }
    }

    public Det()
    {
    }
  }
}
