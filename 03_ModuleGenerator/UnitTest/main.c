/*****************************************************************************************************************************
* @file        main.c
* @author      Reinemuth Sebastian
* @date        02-09-2017
* @brief       Module Test for Module DET
* @version     1.0.0
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
****************************************************************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

/* Private includes -------------------------------------------------------------------------------------------------------*/
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>

/* Module Header */
#include "Det.h"
#include "Det_PBCfg.h"

/* Mocked Data Types -----------------------------------------------------------------------------------------------------*/
/**
* @struct   detUserDeviations_t
* @brief    Copy of source data type struct >>Struct definitions of the neccessary informations to deviate an det error
*/
typedef struct
{
  uint16 ModuleId;
  uint16 ErrorId;
  boolean used;
}detUserDeviations_t;

/* Global variables ------------------------------------------------------------------------------------------------------*/
extern bswDet_ReleaseDet;
extern detUserDeviations_t detUserDeviations[];

/* Private function prototypes -------------------------------------------------------------------------------------------*/
/**
  * @brief      Testfunction to test if the Init would wourk test return values 
  * @param      none
  * @retval     none
  */
static void TG001_TC001_InitReturnValues(void **state)
{
  Std_ReturnType retVal;
  
  /* >>>>>>>>>>>>>>> TestSetup <<<<<<<<<<<<<<< */

  /* >>>>>>>>>>>>>>> Initialize System <<<<<<<<<<<<<<< */

  /* >>>>>>>>>>>>>>> TEST STARTUP <<<<<<<<<<<<<<< */
  /* Check int return value */
  retVal = Det_InitMemory();
  assert_true(E_OK == retVal);

  /* Check init memory return value */
  Det_Init();
}

/**
  * @brief      Test if internal release variable value is correct after init
  * @param      none
  * @retval     none
  */
static void TG001_TC002_TestInitReleaseDetVarValue(void **state)
{
  /* >>>>>>>>>>>>>>> TestSetup <<<<<<<<<<<<<<< */
  bswDet_ReleaseDet = TRUE;
  assert_true(TRUE == bswDet_ReleaseDet);

  /* >>>>>>>>>>>>>>> Initialize System <<<<<<<<<<<<<<< */
  Det_InitMemory();
  Det_Init();

  /* >>>>>>>>>>>>>>> TEST STARTUP <<<<<<<<<<<<<<< */
  /* Check internal release variable value */
  assert_true(FALSE == bswDet_ReleaseDet);
}

/**
  * @brief      Test if internal deviation variable value is correct after init
  * @param      none
  * @retval     none
  */
static void TG001_TC003_TestInitDeviationVarValue(void **state)
{
  /* >>>>>>>>>>>>>>> TestSetup <<<<<<<<<<<<<<< */
  detUserDeviations[0].used = TRUE;
  detUserDeviations[DET_MAX_DET_USER_DEVIATIONS-1].used = TRUE;
  assert_true(detUserDeviations[0].used == TRUE);

  /* >>>>>>>>>>>>>>> Initialize System <<<<<<<<<<<<<<< */
  Det_InitMemory();
  Det_Init();

  /* >>>>>>>>>>>>>>> TEST STARTUP <<<<<<<<<<<<<<< */
  /* Check deviation memory */
  for (int idx = 0; idx < DET_MAX_DET_USER_DEVIATIONS; idx++)
  {
    /* Just check if */
    assert_true(detUserDeviations[idx].used == FALSE);
  }
}

/**
  * @brief      Test if an new Module could be added to the DET Deviation List
  * @param      none
  * @retval     none
  */
static void TG001_TC004_AddDeviationElement(void **state)
{
  Std_ReturnType retVal;

  /* >>>>>>>>>>>>>>> TestSetup <<<<<<<<<<<<<<< */
  uint16 randNr1 = rand() % 100000;
  uint16 randNr2 = rand() % 100000;

  /* >>>>>>>>>>>>>>> Initialize System <<<<<<<<<<<<<<< */
  Det_InitMemory();
  Det_Init();

  /* >>>>>>>>>>>>>>> TEST STARTUP <<<<<<<<<<<<<<< */
  /* Set Test Deviation Element */
  retVal = Det_AddUserDeviation(randNr1, randNr2);
  assert_true(E_OK == retVal);

  /* Check if Element Exists within deviation list */
  assert_int_equal(randNr1, detUserDeviations[0].ModuleId);
  assert_int_equal(randNr2, detUserDeviations[0].ErrorId);
  assert_true(TRUE == detUserDeviations[0].used);
  assert_true(FALSE == detUserDeviations[1].used);

}

/**
  * @brief      Test if multiple new Modules could be added to the DET Deviation List
  * @param      none
  * @retval     none
  */
static void TG001_TC005_AddMultipleDeviationElements(void **state)
{
  Std_ReturnType retVal;

  /* >>>>>>>>>>>>>>> TestSetup <<<<<<<<<<<<<<< */
  uint16 randNr1 = rand() % 100000;
  uint16 randNr2 = rand() % 100000;
  uint16 randNr3 = rand() % 100000;
  uint16 randNr4 = rand() % 100000;

  /* >>>>>>>>>>>>>>> Initialize System <<<<<<<<<<<<<<< */
  Det_InitMemory();
  Det_Init();

  /* >>>>>>>>>>>>>>> TEST STARTUP <<<<<<<<<<<<<<< */
  /* Set Test Deviation Element */
  retVal = Det_AddUserDeviation(randNr1, randNr2);
  assert_true(E_OK == retVal);
  retVal = Det_AddUserDeviation(randNr3, randNr4);
  assert_true(E_OK == retVal);

  /* Check if Element Exists within deviation list */
  assert_int_equal(randNr1, detUserDeviations[0].ModuleId);
  assert_int_equal(randNr2, detUserDeviations[0].ErrorId);
  assert_true(TRUE == detUserDeviations[0].used);

  assert_int_equal(randNr3, detUserDeviations[1].ModuleId);
  assert_int_equal(randNr4, detUserDeviations[1].ErrorId);
  assert_true(TRUE == detUserDeviations[1].used);

  assert_true(FALSE == detUserDeviations[2].used);

}

/**
  * @brief      Test behaviour if too much new Modules would be added to the DET Deviation List
  * @param      none
  * @retval     none
  */
static void TG001_TC006_AddTooMuchDeviationElements(void **state)
{
  Std_ReturnType retVal;
  /* >>>>>>>>>>>>>>> TestSetup <<<<<<<<<<<<<<< */
  Det_InitMemory();
  Det_Init();

  /* Set dummy elements */
  for (uint16 idx = 0; idx <= DET_MAX_DET_USER_DEVIATIONS -2; idx++)
  {
    retVal = Det_AddUserDeviation(0xAA, 0xBB);
    assert_true(E_OK == retVal);
  }
  
  /* >>>>>>>>>>>>>>> Initialize System <<<<<<<<<<<<<<< */
  /* Set det moudle to deviations list */
  retVal = Det_AddUserDeviation(DET_MODULE_ID, DET_E_DET_DEVIATIONS_FULL);
  assert_true(E_OK == retVal);


  /* >>>>>>>>>>>>>>> TEST STARTUP <<<<<<<<<<<<<<< */
  /* Set one element to much */
  retVal = Det_AddUserDeviation(0xAA, 0xBB);
  assert_false(E_OK == retVal);

}

/**
  * @brief      Test behaviour if one det deviation element shall be removed but ther was never one set
  * @param      none
  * @retval     none
  */
static void TG001_TC007_RemoveOneDeviationElement_NoElementsAvailable(void **state)
{
  Std_ReturnType retVal;

  /* >>>>>>>>>>>>>>> TestSetup <<<<<<<<<<<<<<< */
  Det_InitMemory();
  Det_Init();

  /* >>>>>>>>>>>>>>> Initialize System <<<<<<<<<<<<<<< */

  /* >>>>>>>>>>>>>>> TEST STARTUP <<<<<<<<<<<<<<< */
  retVal = Det_RemoveUserDeviation(0xAA, 0xBB);
  assert_true(E_NOT_OK == retVal);
}

/**
  * @brief      Test behaviour if one det deviation element shall be removed one element is set.
  * @param      none
  * @retval     none
  */
static void TG001_TC008_RemoveOneDeviationElement_OneElementsAvailable(void **state)
{
  Std_ReturnType retVal;
  uint16 randNr1 = rand() % 100000;
  uint16 randNr2 = rand() % 100000;

  /* >>>>>>>>>>>>>>> TestSetup <<<<<<<<<<<<<<< */
  Det_InitMemory();
  Det_Init();

  /* >>>>>>>>>>>>>>> Initialize System <<<<<<<<<<<<<<< */
  /* Set det moudle to deviations list */
  retVal = Det_AddUserDeviation(randNr1, randNr2);
  assert_true(E_OK == retVal);

  /* >>>>>>>>>>>>>>> TEST STARTUP <<<<<<<<<<<<<<< */
  retVal = Det_RemoveUserDeviation(randNr1, randNr2);
  assert_true(E_OK == retVal);
}

/**
  * @brief      Test behaviour if one det deviation element shall be removed two element are set.
  * @param      none
  * @retval     none
  */
static void TG001_TC009_RemoveOneDeviationElement_TwoElementsAvailable(void **state)
{
  Std_ReturnType retVal;
  uint16 randNr1 = rand() % 100000;
  uint16 randNr2 = rand() % 100000;
  uint16 randNr3 = rand() % 100000;
  uint16 randNr4 = rand() % 100000;

  /* >>>>>>>>>>>>>>> TestSetup <<<<<<<<<<<<<<< */
  Det_InitMemory();
  Det_Init();

  /* >>>>>>>>>>>>>>> Initialize System <<<<<<<<<<<<<<< */
  /* Set det moudle to deviations list */
  retVal = Det_AddUserDeviation(randNr1, randNr2);
  assert_true(E_OK == retVal);

  retVal = Det_AddUserDeviation(randNr3, randNr4);
  assert_true(E_OK == retVal);

  /* >>>>>>>>>>>>>>> TEST STARTUP <<<<<<<<<<<<<<< */
  retVal = Det_RemoveUserDeviation(randNr1, randNr2);
  assert_true(E_OK == retVal);

  /* Check det deviatin list elements */
  assert_int_equal(randNr3, detUserDeviations[0].ModuleId);
  assert_int_equal(randNr4, detUserDeviations[0].ErrorId);
  assert_true(TRUE == detUserDeviations[0].used);
  
  /* Check if last element is unused */
  assert_true(FALSE
    == detUserDeviations[1].used);
   
}

/* Functions ------------------------------------------------------------------------------------------------------------*/
/**
  * @brief      Mainfuntion for visual studio projekt
  * @param      none
  * @retval     none
  */
int main(void) 
{
  /* Setup Console for Test Output */
  printf("Startup of Module Tests for Module DET. \r\nTest Framework: CMocka 1.1.1 \r\n\r\n");

  /* Creating tests and run them */
  const struct CMUnitTest TG001_StandardTests[] = {
    cmocka_unit_test(TG001_TC001_InitReturnValues),
    cmocka_unit_test(TG001_TC002_TestInitReleaseDetVarValue),
    cmocka_unit_test(TG001_TC003_TestInitDeviationVarValue),
    cmocka_unit_test(TG001_TC004_AddDeviationElement),
    cmocka_unit_test(TG001_TC005_AddMultipleDeviationElements),
    cmocka_unit_test(TG001_TC006_AddTooMuchDeviationElements),
    cmocka_unit_test(TG001_TC007_RemoveOneDeviationElement_NoElementsAvailable),
    cmocka_unit_test(TG001_TC008_RemoveOneDeviationElement_OneElementsAvailable),
    cmocka_unit_test(TG001_TC009_RemoveOneDeviationElement_TwoElementsAvailable),
  };

  cmocka_run_group_tests(TG001_StandardTests, NULL, NULL);

  /* wait for user key to shutdown system */
  printf("\r\nPres any key to exit test environment \r\n");
  getch();
  return 1;
}
#ifdef __cplusplus
}
#endif